FROM python:3.8-alpine

COPY ./requirements.txt /tmp/requirements.txt

RUN apk add --update \
    && pip install -r /tmp/requirements.txt

ADD . /app
WORKDIR /app

CMD python bouquet.py
