import pytest

from models import Design


DESIGN = 'AL10a15b5c30'
DESIGN_WITH_EXTRA_SPACE = DESIGN[:-1] + '5'
# run the tests for both designs
DESIGNS = (Design(DESIGN), Design(DESIGN_WITH_EXTRA_SPACE))


@pytest.mark.parametrize("design", DESIGNS)
def test_count_bouquets_with_no_flowers(design):
    assert design.count_bouquets({}) == 0


@pytest.mark.parametrize("design", DESIGNS)
def test_count_bouquets_with_flowers_that_dont_exist_in_its_design(design):
    assert design.count_bouquets({'z': 1}) == 0


@pytest.mark.parametrize("design", DESIGNS)
def test_count_bouquets_with_valid_flowers_but_not_enough(design):
    assert design.count_bouquets({'a': 10, 'b': 15, 'c': 4}) == 0


@pytest.mark.parametrize("design", DESIGNS)
def test_count_bouquets_with_enough_valid_flowers(design):
    assert design.count_bouquets({'a': 10, 'b': 15, 'c': 5}) == 1


@pytest.mark.parametrize("design", DESIGNS)
def test_count_bouquets_with_more_than_enough_flowers_but_less_than_one_bouquet(design):
    assert design.count_bouquets({'a': 10, 'b': 15, 'c': 6}) == 1


@pytest.mark.parametrize("design", DESIGNS)
def test_make_bouquet_with_no_extra_flowers(design):
    assert design.make_bouquet() == design._raw_string[:-2]


def test_make_bouquet_with_extra_flowers_with_0_extra_space():
    design = DESIGNS[0]
    assert design.make_bouquet(extra_flowers=['z']) == design._raw_string[:-2]


def test_make_bouquet_with_extra_flowers_but_less_than_designs_limit():
    extra_flowers = ['z'] * (DESIGNS[1].extra_space - 1)
    result = DESIGNS[1].make_bouquet(extra_flowers=extra_flowers)
    assert result == DESIGN[:-2] + f'{len(extra_flowers)}z'


def test_make_bouquet_with_extra_flowers_more_than_designs_limit():
    design = DESIGNS[1]
    extra_flowers = ['z'] * (design.extra_space + 1)
    result = design.make_bouquet(extra_flowers=extra_flowers)
    assert result == DESIGN[:-2] + f'{design.extra_space}z'
