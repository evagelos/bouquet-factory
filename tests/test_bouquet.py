from io import StringIO
import sys

from bouquet import read_data


def test_reading_invalid_designs():
    sys.stdin = StringIO("foo\nbar\n")
    designs, _ = read_data()
    assert designs == []


def test_reading_valid_designs_from_mix_of_invalid_and_valid():
    sys.stdin = StringIO("foo\nbar\nAL10a15b5c30")
    designs, _ = read_data()
    assert len(designs) == 1


def test_reading_invalid_flowers():
    sys.stdin = StringIO("foo\nbar\n")
    _, flowers = read_data()
    assert flowers == {'S': {}, 'L': {}}


def test_reading_valid_flowers_from_mix_of_invalid_and_valid():
    sys.stdin = StringIO("foo\nbar\naS\nbL")
    _, flowers = read_data()
    assert len(flowers['S']) == 1 and len(flowers['L']) == 1
