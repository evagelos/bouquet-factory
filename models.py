import re
from collections import Counter


class Design:

    pattern = r"^[A-Z](S|L)(\d+[a-z])+[0-9]+$"

    def __init__(self, design):
        self._raw_string = design
        self.size = design[1]
        self.species = {
            f[-1]: int(f[:-1])
            for f in re.findall(r"\d+[a-z]", design[2:])
        }
        self.capacity = int(re.search(r"\d+$", design[2:]).group())
        self.extra_space = self.capacity - sum(self.species.values())

    def count_bouquets(self, flowers):
        """Return how many bouquets can be made given some flowers."""
        return min([
            flowers.get(specie, 0) // amount
            for specie, amount in self.species.items()
        ])

    def make_bouquet(self, extra_flowers=None):
        """Return the design using any extra_flowers up to design's extra space limit."""
        if extra_flowers:
            extra_flowers = extra_flowers[:self.extra_space]
        extra_flowers_string = (
            f'{times}{specie}'
            for specie, times in Counter(extra_flowers or []).items()
        )

        return self._raw_string[:-2] + ''.join(extra_flowers_string)

    def __contains__(self, specie):
        """Return if specie is part of the design."""
        return specie in self.species
