# Bouquet factory

Print bouquets given designs and flowers

### How to install
```sh
$ pip install -r requirements.txt
```

### How to run
```sh
$ python bouquet.py < sample.txt
```

### Run tests
```sh
$ pytest
```

## Run the app using docker
```sh
$ docker build -t bouquet .
$ docker run -i bouquet < sample.txt
```
