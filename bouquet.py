import sys
import re
from collections import defaultdict
from itertools import chain

from models import Design


def read_data():
    designs = []
    flowers = {'S': defaultdict(int), 'L': defaultdict(int)}

    for line in sys.stdin:
        if re.match(Design.pattern, line):
            designs.append(Design(line))
        elif re.match(r"^[a-z](S|L)$", line):
            flowers[line[1]][line[0]] += 1

    return designs, flowers


def select_best_design(designs, flowers):
    """Return the best design that will use most of the flowers."""
    counters = [d.count_bouquets(flowers) * d.capacity for d in designs]
    highest = max(counters or [0])
    if highest == 0:
        return None
    index = counters.index(highest)
    return designs[index]


def produce_bouquets(designs, flowers):
    """Return bouquets created updating also the flower quantities."""
    bouquets = defaultdict(int)
    while design := select_best_design(designs, flowers):
        times = design.count_bouquets(flowers)
        bouquets[design] += times
        for specie, amount in design.species.items():
            flowers[specie] -= times * amount

    return bouquets


def main():
    designs, flowers_per_size = read_data()
    for size, flowers in flowers_per_size.items():
        bouquets = produce_bouquets([d for d in designs if d.size == size], flowers)
        remaining_flowers = list(chain(*[k * v for k, v in flowers.items() if v > 0]))
        for design, times in bouquets.items():
            for _ in range(times):
                string = design.make_bouquet(extra_flowers=remaining_flowers)
                del remaining_flowers[:design.extra_space]
                print(string)


if __name__ == '__main__':
    main()
